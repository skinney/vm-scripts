#!/bin/bash

if [ $# -ne 2 ]; then
	  echo "virt-install-wrapper.sh name path_to_iso"
	  exit 65
fi

virt-install  \
    --name $1 \
    --os-type=linux \
    --memory 1024             \
    --vcpus=2,maxvcpus=4      \
    --cpu host                \
    --cdrom $2 \
    --disk size=12,format=raw,bus=virtio \
    --network bridge=br1,model=virtio \
    --graphics vnc,listen=0.0.0.0 --noautoconsole \
    --virt-type kvm

