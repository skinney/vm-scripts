#!/bin/bash
if [ $# -ne 2 ]; then
	echo "virt-customize-script.sh domain_name hostname"
	exit 65
fi

virt-customize \
-d $1 \
--hostname $2 \
--verbose \
--update \
--delete /etc/salt/pki/minion/minion_master.pem \
--delete /etc/salt/minion_id \
--install salt-common,salt-minion \
--upload /etc/ssh/ssh_host_rsa_key:/etc/ssh/ssh_host_rsa_key \
--upload /etc/ssh/ssh_host_rsa_key.pub:/etc/ssh/ssh_host_rsa_key.pub 



