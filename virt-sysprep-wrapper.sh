#!/bin/bash
if [ $# -ne 1 ]; then
	echo "virt-sysprep-script.sh domain_name"
	exit 65
fi

virt-sysprep -d $1 --verbose \
	     --delete '/etc/salt/minion_id' \
	     --delete '/etc/salt/pki/minion/*' \
	     --firstboot-command '/usr/bin/salt-call state.sls users'
