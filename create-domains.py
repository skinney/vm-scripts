#!/usr/bin/env python
'''\
Usage: create_domains.py [--count NUM] [--base_name NAME] [--start_num NUM]

Options:
 --count NUM        The number of guests to launch [default: 1]
 --start_num NUM    Sequence number to start with [default: 0]
 --base_name NAME   The name. This scritp will add a count to the name ie vm1 [default: vm]
 --help             Display this screen

'''
import sys, os
import random
import uuid
import xml.etree.ElementTree as ET
import docopt
import subprocess32
import libvirt

template_file = 'domain-template.xml'
storage_pool_path = '/var/lib/libvirt/images/guest_images_fs'
base_image = 'ubuntu_1404_backing_img.qcow2'

args = docopt.docopt(__doc__)
count = args['--count']

def customize_guest(name):
    print '[+] Customizing {} image'.format(name)

    cmd_list = ['virt-customize', '-d', name, '--hostname', name,
                '--upload', '/etc/ssh/ssh_host_rsa_key:/etc/ssh/ssh_host_rsa_key',
                '--upload', '/etc/ssh/ssh_host_rsa_key.pub:/etc/ssh/ssh_host_rsa_key.pub']

    out, err = shell_out(cmd_list)

    if err:
        print '[-] virt-customize returned an error:\n{}'.format(err)
    else:
        print out


def create_snapshot(name):
    print '[+] Creating image for {}'.format(name)

    if os.path.isfile('{}/{}'.format(storage_pool_path, base_image)):
        cmd_list = ['qemu-img', 'create', '-f', 'qcow2', '-b',
                    '{}/{}'.format(storage_pool_path, base_image),
                    '{}/{}.qcow2'.format(storage_pool_path, name)]

        out, err = shell_out(cmd_list)

        if err:
            print '[-] QEMU returned an error creating qemu snapshot:\n{}'.format(err)
            sys.exit(1)
        else:
            print out
    else:
        print 'Error: {} no such file'.format(base_image)
        sys.exit(1)


def shell_out(cmd_list):
    sub_out = subprocess32.Popen(cmd_list, stdin=None, stdout=subprocess32.PIPE,
                                 stderr=subprocess32.PIPE)
    return sub_out.communicate()



def create_domain_xml(name):
    print '[+] Creating new domain: {}'.format(name)

    if os.path.exists(template_file):
        tree = ET.parse(template_file)
        root = tree.getroot()

    # set name
    print 'setting name: {}'.format(name)
    element_name = root.find('name')
    element_name.text = name

    # set uuid
    print 'setting uuid'
    _uuid = uuid.uuid1()
    print _uuid
    element_uuid = root.find('uuid')
    element_uuid.text = str(_uuid)

    # set devices, disk, driver, type
    # set devices, disk, source
    # set net mac addr
    for child in root:

        if child.tag == 'devices':

            devices = child

            for device in devices:

                if device.tag == 'disk':

                    disk = device

                    if disk.attrib['type'] == 'file':

                        disk_file = disk

                        for i in disk_file:

                            if i.tag == 'driver':
                                i.attrib['type'] = 'qcow2'

                            if i.tag == 'source':
                                i.attrib['file'] = '{}/{}.qcow2'.format(storage_pool_path, name)

                if device.tag == 'interface':
                    net_interface = device

                    for i in net_interface:
                        if i.tag == 'mac':
                            i.attrib['address'] = mac_pretty_print(random_mac())

    tree.write('{}.xml'.format(name))


def define_domain(conn, name):
    print '[+] Defining domain {}'.format(name)

    with open('{}.xml'.format(name), 'r') as file:
        xml = file.read()

    try:
        res = conn.defineXML(xml)
    except:
        print '[-] Error defining domain {}'.format(name)
        sys.exit(1)
    finally:
        os.remove('{}.xml'.format(name))


def start_vm(name, conn):
    print '[+] Starting {}'.format(name)
    try:
        virtual_machine = conn.lookupByName(name)
    except:
        print '[-] Could not find {}'.format(name)
        sys.exit(1)

    try:
        res = virtual_machine.create()
    except:
        print '[-] Could not create {}'.format(name)
        sys.exit(1)


def random_mac():
    return [0x00, 0x16, 0x3e,
            random.randint(0x00, 0x7f),
            random.randint(0x00, 0xff),
            random.randint(0x00, 0xff)]


def mac_pretty_print(mac):
    return ':'.join(map(lambda x: "%02x" % x, mac))


#
#

conn = libvirt.open('qemu:///system')

for index in range(int(count)):

    if args['--start_num']:
        index += int(args['--start_num'])

    name = '{}{}'.format(args['--base_name'], str(index))

    create_snapshot(name)

    create_domain_xml(name)

    define_domain(conn, name)

    customize_guest(name)

    start_vm(name, conn)
