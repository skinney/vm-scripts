#!/usr/bin/env python
import sys
import libvirt
import xml.etree.ElementTree as ET

mac = sys.argv[1]

def findDomainWithMac(conn, addr):
    domlist = map(conn.lookupByID, conn.listDomainsID())
    for dom in domlist:
        root = ET.fromstring(dom.XMLDesc())
        searchString = "./devices/interface/mac[@address='{0}']".format(addr)
        if (root.find(searchString) is not None):
            return dom

conn = libvirt.openReadOnly('qemu:///system')
domain = findDomainWithMac(conn, mac)
print mac
print domain.name()
